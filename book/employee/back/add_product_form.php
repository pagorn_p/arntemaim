<?
include ("utility.php");
include ("../chksession2.php");
connect_db();

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>อ่านเต็มอิ่ม - เพิ่มหมวดหมู่สินค้า</title>

<style type="text/css">
a:link {
	color: #786A39;
	text-decoration: none;
}
a:hover {
	color: #FF0080;
	text-decoration: none;
}
.head_cat h2 {
	font-size: 24px;
}
.zzz h2 {
	color: #E90B48;
	font-family: Tahoma, Geneva, sans-serif;
}
.hhh {
	font-weight: bold;
	color: #B9B343;
}
a:visited {
	text-decoration: none;
}
a:active {
	text-decoration: none;
	color: #FF0080;
}
.bb {
	font-weight: bold;
	font-size: 14px;
}
#apDiv1 {
	position: absolute;
	left: 127px;
	top: 196px;
	width: 100px;
	height: 77px;
	z-index: 1;
	color: #FFF;
}
#pagot_white {
	font-size: 24px;
	color: #FFF;
	text-align: center;
}
body,td,th {
	font-family: Tahoma, Geneva, sans-serif;
}
</style>
</head>

<body>
<a href="../main.php">หน้าหลัก</a>
<div align="center" class="zzz">
<h2>เพิ่มหนังสือ</h2></div>
<form methid="post" action="add_product.php">
<table width="41%" border="1" align="center" cellpadding="5" cellspacing="1" >
<tr>
<td bgcolor="#DDB37D" class="bb">ISBN</td>
<td><input name = "isbn" type="text" maxlength="13" /></td>
</tr>
<tr>
<td bgcolor="#DDB37D" class="bb">ชื่อหนังสือ</td>
<td><input name = "name" type="text" maxlength="100" /></td>
</tr>
<tr>
<td bgcolor="#DDB37D" class="bb">เล่มที่(ถ้ามี)</td>
<td><input name = "vol" type="text" size="3" maxlength="3" /></td>
</tr>
<tr>
<td bgcolor="#DDB37D" class="bb">ราคา</td>
<td><input name = "price" type="text" size="5" maxlength="4" /></td>
</tr>
<tr>
<td bgcolor="#DDB37D" class="bb">สำนักพิมพ์</td>
<td><input name = "publish" type="text" maxlength="30" /></td>
</tr>
<tr>
<td bgcolor="#DDB37D" class="bb">จำนวนทั้งหมด</td>
<td><input name = "amount" type="text" size="3" maxlength="2" /></td>
</tr>
<tr>
  <td bgcolor="#DDB37D" class="bb">รหัสหมวดหมู่</td>
  <td><label for="type_id"></label>
    <select name="type">
      <option value=""><-- Please Select Item --></option>
      <?
  $strSQL = "select * from booktype order by TId ASC";
  $objQuery = mysql_query($strSQL);
  while($objResult = mysql_fetch_array($objQuery))
  {
	  ?>
      <option value = "<?=$objResult["TId"];?>"><?=$objResult["TId"]." - ".$objResult["TName"];?></option>
      <?
	 }
	  ?>
      </select>
    
    </td>
</tr>
<tr>
<td bgcolor="#DDB37D" class="bb">รหัสผู้เขียน</td>

<td valign="middle"><label for="acode_drop"></label>
  <select name="acode_drop_two" size="10">
    <?
  $strSQL = "select * from author order by ACode ASC";
  $objQuery = mysql_query($strSQL);
  while($objResult = mysql_fetch_array($objQuery))
  {
	  ?>
      <option value = "<?=$objResult["ACode"];?>" selected="selected"><?=$objResult["ACode"]." --- ".$objResult["AFName"]." ".$objResult["ALName"]." : ".$objResult["APenName"];?></option>
      <?
	 }
	  ?>
  </select>  
  <a href="#" onClick="MyWindow=window.open('add_author_form.php','MyWindow','toolbar=no,location=yes,directories=yes,status=no,menubar=no,scrollbars=yes,resizable=yes,width=600,height=300'); return false;">+เพิ่มผู้แต่งใหม่</a></td>
</tr>


<tr>
<td colspan="2" align="center">
<input type="submit" value="เพิ่ม" />&nbsp;<input type = "reset" value="เคลียร์" />
</td>
</tr>
</table>
</form>
</body>
</html>
<?
mysql_close();
?>