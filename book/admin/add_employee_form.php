<?
include ("back/utility.php");
include ("chksession.php");
connect_db();

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>อ่านเต็มอิ่ม - เพิ่มพนักงาน</title>

<style type="text/css">
a:link {
	color: #786A39;
	text-decoration: none;
}
a:hover {
	color: #FF0080;
	text-decoration: none;
}
.head_cat h2 {
	font-size: 24px;
}
.zzz h2 {
	color: #E90B48;
	font-family: Tahoma, Geneva, sans-serif;
}
.hhh {
	font-weight: bold;
	color: #B9B343;
}
a:visited {
	text-decoration: none;
}
a:active {
	text-decoration: none;
	color: #FF0080;
}
.bb {
	font-weight: bold;
	font-size: 14px;
}
#apDiv1 {
	position: absolute;
	left: 127px;
	top: 196px;
	width: 100px;
	height: 77px;
	z-index: 1;
	color: #FFF;
}
#pagot_white {
	font-size: 24px;
	color: #FFF;
	text-align: center;
}
body,td,th {
	font-family: Tahoma, Geneva, sans-serif;
}
</style>
</head>

<body>
<a href="main.php">หน้าหลัก</a>
<div align="center" class="zzz">
<h2>เพิ่มพนักงาน</h2></div>
<form methid="post" action="add_employee.php">
<table width="41%" border="1" align="center" cellpadding="5" cellspacing="1" >
<tr>
<td bgcolor="#DDB37D" class="bb">ชื่อ</td>
<td><input name = "name" type="text" maxlength="20" /></td>
</tr>
<tr>
<td bgcolor="#DDB37D" class="bb">นามสกุล</td>
<td><input name = "lname" type="text" maxlength="30" /></td>
</tr>
<tr>
<td bgcolor="#DDB37D" class="bb">ID Card</td>
<td><input name = "ssn" type="text" maxlength="13" /></td>
</tr>
<tr>
<td bgcolor="#DDB37D" class="bb">เพศ</td>
<td><INPUT NAME="gender" TYPE="radio" value="ชาย" checked>ชาย 
        <INPUT NAME="gender" TYPE="radio" value="หญิง">หญิง</td>
</tr>
<tr>
<td bgcolor="#DDB37D" class="bb">ที่อยู่</td>
<td><textarea name="add"></textarea></td>
</tr>
<tr>
<td bgcolor="#DDB37D" class="bb">โทรศัพท์</td>
<td><input name = "tel" type="text" maxlength="10" /></td>
</tr>
<tr>
  <td bgcolor="#DDB37D" class="bb">สถานะ</td>
  <td><INPUT NAME="status" TYPE="radio" value="ทำงาน" checked>ทำงาน
    <INPUT NAME="status" TYPE="radio" value="ลาออก">ลาออก</td>
</tr>


<tr>
  <td colspan="2" align="center">
  <input type="submit" value="เพิ่ม" />&nbsp;<input type = "reset" value="เคลียร์" />
  </td>
</tr>
</table>
</form>
</body>
</html>
<?
mysql_close();
?>